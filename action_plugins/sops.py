from ansible.plugins.action.include_vars import ActionModule as IncludeVarsModule
from ansible.module_utils._text import to_text
import os
import subprocess
import string
import random

class ActionModule(IncludeVarsModule):

    def _which(self, program):
        def is_exe(fpath):
            return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

        fpath, fname = os.path.split(program)
        if fpath:
            if is_exe(program):
                return program
        else:
            for path in os.environ["PATH"].split(os.pathsep):
                path = path.strip('"')
                exe_file = os.path.join(path, program)
                if is_exe(exe_file):
                    return exe_file

        return None

    def run(self, tmp=None, task_vars=None):

        failed = False
        err_msg = ""
        self.included_files = []

        self.sops_executable = self._task.args.get('sops_executable', None)
        if self.sops_executable is None:
            self.sops_executable = self._which('sops')
            if self.sops_executable is None:
                failed = True
                err_msg = "Could not find sops in the current $PATH. Is it installed?"

        if failed:
            result = {}
            result['failed'] = failed
            result['message'] = err_msg
            return result

        return super(ActionModule, self).run(tmp=tmp, task_vars=task_vars)

    def random_generator(self, size=6, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for x in range(size))

    def _load_files(self, filename, validate_extensions=False):
        """ Loads a file and converts the output into a valid Python dict.
        Args:
            filename (str): The source file.
        Returns:
            Tuple (bool, str, dict)
        """
        results = dict()
        failed = False
        err_msg = ''
        if validate_extensions and not self._is_valid_file_ext(filename):
            failed = True
            err_msg = ('{0} does not have a valid extension: {1}' .format(filename, ', '.join(self.valid_extensions)))
        else:

            args = [self.sops_executable, '-d', filename]
            p = subprocess.Popen(args, env=os.environ, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            p.wait()
            stdout = p.stdout.read()
            stderr = p.stderr.read()
            exit_code = p.returncode

            if exit_code != 0:
                failed = True
                err_msg = ('Unable to decrypt file {0}. Error Message:' .format(filename, stderr))
                return failed, err_msg, results

            dec_filename = filename+"."+self.random_generator()+".dec"
            with open(dec_filename, 'w+') as f:
                f.write(stdout)

            b_data, show_content = self._loader._get_file_contents(dec_filename)

            os.remove(dec_filename)

            data = to_text(b_data, errors='surrogate_or_strict')

            self.show_content = show_content
            data = self._loader.load(data, show_content)
            if not data:
                data = dict()
            if not isinstance(data, dict):
                failed = True
                err_msg = ('{0} must be stored as a dictionary/hash' .format(filename))
            else:
                self.included_files.append(filename)
                results.update(data)

        return failed, err_msg, results
